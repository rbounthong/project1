//
//  MyWS.m
//  Projet1
//
//  Created by Rémy Bounthong on 22/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import "MyWS.h"
#import "AFNetworking.h"

@implementation MyWS

+ (MyWS *)sharedInstance
{
    static MyWS *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MyWS alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void)getMyJSON:(void (^)(NSArray *list))finished
{
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	
	[manager GET:@"http://www.json-generator.com/api/json/get/bQIMCfPNMy?indent=2"
	  parameters:nil
		 success:^(AFHTTPRequestOperation *operation, id responseObject)
	{
		NSArray *myArray = responseObject;
        finished(myArray);
	}
		 failure:^(AFHTTPRequestOperation *operation, NSError *error)
	{
		finished(nil);
	}];
}

@end
