//
//  main.m
//  Projet1
//
//  Created by Rémy Bounthong on 21/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
