//
//  DetailsViewController.m
//  Projet1
//
//  Created by Rémy Bounthong on 23/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    /*
     NSDictionary *myObject = _myArray[indexPath.row];
     
     cell.cellName.text = [NSString stringWithFormat:@"Name : %@", myObject[@"name"]];
     cell.cellDetails.text = [NSString stringWithFormat:@"Gender : %@", myObject[@"gender"]];
     NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:myObject[@"picture"]]];
     cell.cellImage.image = [UIImage imageWithData:imageData];
     */
    NSDictionary *myObject = _myArray[self.myRow];

    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:myObject[@"picture"]]];
    self.DetailPicture.image = [UIImage imageWithData:imageData];
    
    self.DetailCompany.text = [NSString stringWithFormat:@"Company: %@", myObject[@"company"]];
    self.DetailName.text = [NSString stringWithFormat:@"Name: %@", myObject[@"name"]];
    self.DetailGender.text = [NSString stringWithFormat:@"Gender: %@", myObject[@"gender"]];
    self.DetailAge.text = [NSString stringWithFormat:@"Age: %@", myObject[@"age"]];
    self.DetailEmail.text = [NSString stringWithFormat:@"Email: %@", myObject[@"email"]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendMail:(id)sender {
    
    NSDictionary *myObject = _myArray[self.myRow];

    // Email Subject
    NSString *emailTitle = @"Test Email";
    // Email Content
    NSString *messageBody = @"iOS programming is so fun!";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:myObject[@"email"]];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
