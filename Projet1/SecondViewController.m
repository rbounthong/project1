//
//  SecondViewController.m
//  Projet1
//
//  Created by Rémy Bounthong on 21/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import "SecondViewController.h"
#import "CustomCell.h"
#import "DetailsViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController


- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_myArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *customCellIdentifier = @"CustomCell";
    
    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSDictionary *myObject = _myArray[indexPath.row];
	
    cell.cellName.text = [NSString stringWithFormat:@"Name : %@", myObject[@"name"]];
    cell.cellDetails.text = [NSString stringWithFormat:@"Gender : %@", myObject[@"gender"]];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:myObject[@"picture"]]];
    cell.cellImage.image = [UIImage imageWithData:imageData];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.row = indexPath.row;
    [self performSegueWithIdentifier:@"PushToDetailsView" sender:nil];

}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"PushToDetailsView"]) {
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"PushToDetailsView"]) {
        
        DetailsViewController *controller = segue.destinationViewController;
        controller.myArray = self.myArray;
        controller.myRow = self.row;
    }
}


@end
