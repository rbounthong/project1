//
//  MyWS.h
//  Projet1
//
//  Created by Rémy Bounthong on 22/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyWS : NSObject

+ (MyWS *)sharedInstance;

- (void)getMyJSON:(void (^)(NSArray *list))finished;

@end
