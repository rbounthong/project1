//
//  AppDelegate.h
//  Projet1
//
//  Created by Rémy Bounthong on 21/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

