//
//  SecondViewController.h
//  Projet1
//
//  Created by Rémy Bounthong on 21/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) NSArray *myArray;
@property (nonatomic, assign) int row;

@end
