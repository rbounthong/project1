//
//  CustomCell.h
//  Projet1
//
//  Created by Rémy Bounthong on 23/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UILabel *cellName;
@property (weak, nonatomic) IBOutlet UILabel *cellDetails;

@end
