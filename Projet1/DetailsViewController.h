//
//  DetailsViewController.h
//  Projet1
//
//  Created by Rémy Bounthong on 23/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface DetailsViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}
/* list info  */
@property (strong, nonatomic) NSArray *myArray;
@property (nonatomic, assign) int myRow;

/* Details Label */
@property (weak, nonatomic) IBOutlet UIImageView *DetailPicture;
@property (weak, nonatomic) IBOutlet UILabel *DetailCompany;
@property (weak, nonatomic) IBOutlet UILabel *DetailName;
@property (weak, nonatomic) IBOutlet UILabel *DetailGender;
@property (weak, nonatomic) IBOutlet UILabel *DetailAge;
@property (weak, nonatomic) IBOutlet UILabel *DetailEmail;

/* Email button */
- (IBAction)sendMail:(id)sender;
@end
