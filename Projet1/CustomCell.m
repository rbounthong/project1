//
//  CustomCell.m
//  Projet1
//
//  Created by Rémy Bounthong on 23/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    // Initialization code
    self.cellImage.frame = CGRectMake(8,8,58,30);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
