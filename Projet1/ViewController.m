//
//  ViewController.m
//  Projet1
//
//  Created by Rémy Bounthong on 21/11/2014.
//  Copyright (c) 2014 Rémy Bounthong. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "SecondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tappedJson:(id)sender {
		
    [[MyWS sharedInstance] getMyJSON:^(NSArray *list) {
        self.list = list;
        [self performSegueWithIdentifier:@"pushToSecondViewController" sender:nil];
    }];
    

    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
	if ([identifier isEqualToString:@"pushToSecondViewController"]) {
        return NO;
	}
	return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"pushToSecondViewController"]) {
        
        SecondViewController *controller = segue.destinationViewController;
        controller.myArray = self.list;
    }
}

@end
